import com.github.play2war.plugin._
import io.apigee.trireme.node10.trireme.tls
import play.PlayImport._

name := """pi-monitor"""

version := "1.0"

Play2WarPlugin.play2WarSettings

Play2WarKeys.servletVersion := "3.0"

Play2WarKeys.targetName := Option("cdc-platform")

lazy val root = (project in file(".")).enablePlugins(PlayJava)

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

autoScalaLibrary := true

libraryDependencies ++= Seq(
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  javaJdbc,
  javaWs,
  filters,
  "org.hibernate" % "hibernate-entitymanager" % "4.3.8.Final",
  "mysql" % "mysql-connector-java" % "5.1.38",
  "org.javassist" % "javassist" % "3.18.2-GA" force(),
  "xalan" % "serializer" % "2.7.2",
  "org.apache.commons" % "commons-email" % "1.2",
  "com.notnoop.apns" % "apns" % "1.0.0.Beta6",
  "commons-io" % "commons-io" % "2.4"
)
/*
smtp {
  host = smtp.gmail.com;
  port = 587;
  user = "mygmail@gmail.com";
  password = "mypassword";
  from = "John User mygmail@gmail.com";
  tls = yes
  ssl = no
}*/