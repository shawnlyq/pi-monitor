package services;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

public class ResponseService {

    private static final String RESULT_SUCCESS = "success";
    private static final String RESULT_ERROR = "error";
    private static final String RESULT_VALIDATION_ERROR = "validation-error";

    public static final String MISSING_FIELD = "missing-field";


    private static ObjectNode buildResponse(String result) {
        ObjectNode responseObject = Json.newObject();
        responseObject.put("result", result);
        return responseObject;
    }


}
