package services;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import play.Logger;
import play.api.Play;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by shawnlaw on 4/7/16.
 */
public class MailService {


    public static final String EMAIL_NAME = "{{NAME}}";
    public static final String EMAIL_LINK = "{{LINK}}";
    public static final String EMAIL_PASSWORD = "{{PASSWORD}}";

    private static HtmlEmail createHtmlEmail() {

        Config configuration = ConfigFactory.load("application.conf");
        String smtpHostName = configuration.getString("mail.smtp.host");
        int smtpPort = configuration.getInt("smtp.port");
        boolean ssl = configuration.getBoolean("smtp.ssl");
        String smtpUser = configuration.getString("mail.smtp.user");
        String smtpPassword = configuration.getString("mail.smtp.pass");

        HtmlEmail htmlEmail = new HtmlEmail();
        htmlEmail.setSmtpPort(smtpPort);
        htmlEmail.setHostName(smtpHostName);


        htmlEmail.setSSL(ssl);
        htmlEmail.setAuthentication(smtpUser, smtpPassword);

        return htmlEmail;
    }

    private static void sendEmail(String recipient, String subject, String message) throws EmailException {

        HtmlEmail email = createHtmlEmail();
        email.addTo(recipient);
        email.setFrom("c200cw@gmail.com");
        email.setSubject(subject);
        email.setHtmlMsg(message);
        email.send();

    }

    public static void sendEmailStaffFirstLoginEmail(String name, String email, String password) {

        try {

            Config configuration = ConfigFactory.load("application.conf");
            //String appHostName = configuration.getString("test host name");

            String subject = "Greetings from CDC North West";
            //String activationUrl = "http://www." + appHostName + "/api/validate?userId=" + user.getId();
            String activationUrl = "http://localhost:9000/#/staffactivate/" + email;
            File file = Play.current().getFile("conf/resources/staff_email_template.html");
            FileReader fileReader = new FileReader(file);
            char[] characters = new char[(int) file.length()];
            fileReader.read(characters);
            String message = new String(characters);
            //String message = "Your auto-generated password is " + password;
            fileReader.close();

            //message = message.replace(EMAIL_NAME, user.getFirstName());
            message = message.replace(EMAIL_LINK, activationUrl);
            message = message.replace(EMAIL_NAME, name);
            message = message.replace(EMAIL_PASSWORD, password);
            //for test purposes
            //email = "c200cw@gmail.com";

            MailService.sendEmail(email, subject, message);

        } catch (EmailException e) {
            Logger.error(e.getMessage());
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }
    }

    public static void sendEmailUserFirstLoginEmail(String name, String email, String password) {

        try {

            Config configuration = ConfigFactory.load("application.conf");
            //String appHostName = configuration.getString("test host name");

            String subject = "Greetings from CDC North West";

            File file = Play.current().getFile("conf/resources/user_email_template.html");
            FileReader fileReader = new FileReader(file);
            char[] characters = new char[(int) file.length()];
            fileReader.read(characters);
            String message = new String(characters);
            //String message = "Your auto-generated password is " + password;
            fileReader.close();

            //message = message.replace(EMAIL_NAME, user.getFirstName());
            //message = message.replace(EMAIL_LINK, activationUrl);
            message = message.replace(EMAIL_NAME, name);
            message = message.replace(EMAIL_PASSWORD, password);
            //for test purposes
            //email = "c200cw@gmail.com";

            MailService.sendEmail(email, subject, message);

        } catch (EmailException e) {
            Logger.error(e.getMessage());
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }
    }

}
