package models;

/**
 * Created by shawnlyq on 20/11/16.
 */

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import play.db.jpa.JPA;
import java.util.List;

@Entity
@Table(name = "data")
public class Data{
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy="uuid2")
    protected String id;

    @Column(name="temperature", nullable = false)
    protected String temperature;

    @Column(name="ldr", nullable = false)
    protected String ldr;

    @Column(name="photo_URL", nullable = false)
    protected String photo_URL;

    @Column(name="var_resistor", nullable = false)
    protected String var_resistor;

    @CreationTimestamp
    @Column(name="date_created", nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    protected Date dateCreated;

    public String getLdr() {
        return ldr;
    }

    public void setLdr(String ldr) {
        this.ldr = ldr;
    }

    public String getPhoto_URL() {
        return photo_URL;
    }

    public void setPhoto_URL(String photo_URL) {
        this.photo_URL = photo_URL;
    }

    public String getVar_resistor() {
        return var_resistor;
    }

    public void setVar_resistor(String var_resistor) {
        this.var_resistor = var_resistor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
