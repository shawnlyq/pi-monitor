package controllers;
import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class MainController extends Controller {

    public static Result index() {
        final JsonNode jsonResponse = Json.toJson("Your new application is ready.");
        return ok(jsonResponse);
    }

    public static Result sayHelloWorld() {
        final JsonNode jsonResponse = Json.toJson("Hello World");
        return ok(jsonResponse);
    }

    @Transactional(readOnly = true)
    public static Result resetCDC() {

        final JsonNode jsonResponse = Json.toJson("Your application data has been reset.");
        return ok(jsonResponse);
    }

    @Transactional
    public static Result createInitialResponseType() {
        //Response_Type.createInitialResponseType();
        return ok("Success");
    }
}
