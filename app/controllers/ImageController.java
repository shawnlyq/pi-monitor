package controllers;

import org.apache.commons.io.FileUtils;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.request;
import static play.mvc.Results.ok;

/**
 * Created by shawnlyq on 22/11/16.
 */
public class ImageController {

    public static Result uploadImage() {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart picture = body.getFile("picture");
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();

            try {
                FileUtils.moveFile(file, new File("images", fileName));
            } catch (IOException ioe) {
                System.out.println("Problem operating on filesystem");
            }
            return ok(fileName);
        } else {
            //flash("error", "Missing file");
            //return redirect(routes.Application.index());
            return ok("File is missing");
        }
    }

    public static Result getImage() {
        return ok("");
    }

}
