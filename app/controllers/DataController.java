package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Data;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.request;
import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

/**
 * Created by shawnlyq on 20/11/16.
 */
public class DataController {
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result addData(){
        JsonNode requestData = request().body().asJson();
        Data data = new Data();
        data.setTemperature(requestData.get("temperature").textValue());
        data.setPhoto_URL(requestData.get("photo_URL").textValue());
        data.setVar_resistor(requestData.get("var_resistor").textValue());
        data.setLdr(requestData.get("ldr").textValue());

        JPA.em().persist(data);
        JPA.em().flush();

        return ok(data.getId());
    }
}
